<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Validation</title>
</head>
<body>
	<?php
		// Opening connexion to database.
		require('database-access.php');

		function RetrieveUserIP()
		{
			if(!empty($_SERVER['HTTP_CLIENT_IP']))
			{
				$address=$_SERVER['HTTP_CLIENT_IP'];
			}
			elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
			{
				$address=$_SERVER['HTTP_X_FORWARDED_FOR'];
			}
			else
			{
				$address=$_SERVER['REMOTE_ADDR'];
			}
			return $address;
		}

		// Getting User IP
		$userIP = RetrieveUserIP();;

		// Getting dates.
		$today = date_create_immutable();
		$maxDate = $today->sub(new DateInterval( "P18Y" ));
		$birthDate = date_create_from_format('Y-m-d H:i:s', $_POST['birth'].' 00:00:00');

		$isDataCorrect = true;

		// Checking phone number format is correct.
		if (strlen($_POST['phone']) != 10 ) {
			echo 'Numéro de téléphone incorect. <br>';
			$isDataCorrect = false;
		}

		// Checking if question is at least 15 characters.
		if (strlen($_POST['question'] < 15)) {
			echo 'Question incorrecte. <br>';
			$isDataCorrect = false;
		}

		// Checking if new user is over 18.
		if ($birthDate > $maxDate)
		{
			echo 'Age minimum requis non atteint. <br>';
			$isDataCorrect = false;
		}

		$id = null;
		$user = null;
		$canChange = true;
		$sql = "SELECT * FROM form_data.users;";
		$result = $connexion->query($sql);
		
		foreach ($result->fetch_all() as $value) {
			/*echo '<pre>';
			var_dump($value);
			echo '</pre>';*/

			if ($value[8] === $userIP) {
				$id = $value[0];
				$user = $value;
				$userCounter = (int)$variable[11] + 1;
			}
		}
		
  		$userUpdatedAt = date_create_from_format('Y-m-d H:i:s', $user[10]);

  		if($userUpdatedAt > $today->sub(new DateInterval( "PT24H" ))) {
  			echo 'Utilisateur modifié il y a moins de 24h. Impossible de le modifier de nouveau avant d\' attendre que ce délai soit terminé';
  			$canChange = false;
  		}

  		if ($canChange) {
  			// Edit user if it already exists.
  			if ($user !== null) {
  				$sql = "UPDATE form_data.users SET firstname = '".$_POST['firstname']."', lastname = '".$_POST['lastname']."', type = '".$_POST['type']."', email = '".$_POST['email']."', birth = '".$_POST['birth']."', phone = '".$_POST['phone']."', country = '".$_POST['country']."', counter = ".$userCounter.", updatedAt = CURRENT_TIME() WHERE id = ".$id.";";

  				$connexion->query($sql);

  				echo 'Utilisateur mis à jour.';

  			// Add user if it doesn't already exists.
  			} else {
  				$sql = "INSERT INTO form_data.users (firstname, lastname, type, email, birth, phone, country, ip, counter, createdAt, updatedAt) VALUES ('".$_POST['firstname']."', '".$_POST['lastname']."', '".$_POST['type']."', '".$_POST['email']."', '".$_POST['birth']."', '".$_POST['phone']."', '".$_POST['country']."','".$userIP."', 0 ,CURRENT_TIME(), CURRENT_TIME());"; 

  				$connexion->query($sql);

  				echo 'Utilisateur créé.';
  			}
  		}

		// Closing connexion to database.
		$connexion->close();
	?>
</body>
</html>
