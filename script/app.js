const birthDate = document.querySelector('#birth');
const maxDate = new Date();
maxDate.setFullYear(maxDate.getFullYear() - 18);
const formattedDate = maxDate.toISOString().split('T')[0];

birthDate.max = formattedDate;
birthDate.value = formattedDate;
