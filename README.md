# Formulaire

## Utilisation
Executer le fichier database.sql pour créer la base de données.
Modifier les accès à la base de données dans le fichier `database-access.php`.
Lancer un serveur de développement à la racine du projet.


Pour ce qui est de l'arhitecture MVC :
 + Le modèle est représenté par la base de données et le fichier `database-access.php` qui en définit les accès.
 + La vue est représentée par le fichier `index.php` qui est la partie que l'utilisateur voie. Il est accompagné des dossiers `style` et `script` contenant respectivement le CSS et le JavaScript. 
 + Le controlleur est représenté par le fichier `validate.php` qui vérifie si un compte utilisateur peut être utilisé / modifié.  

On pourrait imaginer rajouter une page de type "vue" qui indiquera à l'utilsateur l'action qui s'est produite en arrière plan si cela est nécessaire.